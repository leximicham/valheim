#!/bin/bash

# update server's data
steamcmd \
	    +login anonymous \
	        +force_install_dir /valheim/game \
		    +app_update 896660 validate \
		        +quit

#Copy 64bit steamclient, since it keeps using 32bit
cp /root/.steam/steamcmd/linux64/steamclient.so /valheim/game/

#Launch server
export LD_LIBRARY_PATH=./linux64:$LD_LIBRARY_PATH
export SteamAppId=892970
/valheim/game/valheim_server.x86_64 -name "$SERVER_NAME" -port $SERVER_PORT -world "$SERVER_WORLD" -password "$SERVER_PASSWORD" -savedir "$SERVER_SAVEDIR" -public 1 &

#Trap Container Stop for graceful exit
trap "kill -SIGINT $!;" SIGTERM

#Wait for server to exit
while wait $!; [ $? != 0 ]; do true; done
