# Valheim Docker-Compose

A docker-compose ready project that can start a simple and lightweight (as possible) container for running a dedicated Valheim server in Linux on a docker/docker-compose enabled machine. Docker-compose v3.7 or newer.

## Configuration

To run this container you must create a .env file in the root directory. Below is the template for this file. Change only the values on the right (and remove the comments) and not the keys on the left.

### .env Template

```
# ENVIRONMENT=        Appended to container name to differentiate prod vs dev, etc / also server instance and world name
# VALHEIM_FAKE_PORT=  real_port-1 - default 2456
# VALHEIM_REAL_PORT=  Port to reach this site - default 2457
# VALHEIM_EXTRA_PORT= Port 1 above the real port - default 2458
# VALHEIM_GAME=       Volume on local machine to install as the game file (can be used across multiple versions)
# VALHEIM_DATA=       Volume on local machine to install as the data file (individual server saves)
# VALHEIM_PASS=       Server password - must be at least 5 characters long
```

### Network Setup

* The server runs on 3 ports (allegedly); they are sequential to the supplied `VALHEIM_FAKE_PORT`
* All 3 of these ports communicate using UDP
* `VALHEIM_REAL_PORT` is the port that listens for new connections

If you set your `VALHEIM_REAL_PORT` to 2457, this mean you will need to set VALHEIM_FAKE_PORT to 2456 and VALHEIM_EXTRA_PORT to 2458 and your server will be listening on port 2457.

## Starting the Server

While in the project's root directory run `sudo bash nightly.sh` to start the container. See the below section for how to update/restart the server.

## Regular Maintenance

The nightly.sh script needs to be run frequently (nightly preferred) to:

* Check for critical server updates
* Save a backup before updating the game

run this with `sudo crontab -e` and replace xx with the hour of the day you want 15-30 minutes of downtime:
`0 xx * * * bash /absolute_path_to_nightly.sh`

## Custom Save Files

To add an existing save file or to swap out save files, you must name your .db and .fwl both with the `ENVIRONMENT` name in the `VALHEIM_DATA`/worlds directory.

### Admins, Bans, Permits

To add users to the respective admin, ban, and permit lists you can find these in the `VALHEIM_DATA` directory. To add someone to a list, you need their steam64 ids and each player needs their own line in these files

## Troubleshooting

### Docker

Having trouble starting the docker container?

* `sudo docker ps -a | grep valheim` to show any valheim containers
* `sudo docker logs your_container_name` if crashlooping - you can find your container name from the above step
* `sudo docker exec -it your_container_name bash` to enter the container like sshing into a vm (with fewer tools)

### Steam

Can't see your server on the list? I can't either.

In Steam, go to View > Servers and then in the Favorites tab choose Add a Server from the bottom right.
Use your_ip:VALHEIM_REAL_PORT and then Add this address to Favorites.
