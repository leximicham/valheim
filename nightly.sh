#!/bin/bash

#run this with sudo crontab -e
#replace xx with the hour of the day you want 15-30 minutes of downtime
#0 xx * * * bash /absolute_path_to_nightly.sh

#Set cronjob's directory to the directory of the nightly.sh file for environment variables and docker-compose
cd `dirname $0`

#source the environment variable file
source .env

#stop the server and clean up orphaned/anonymous images, networks, containers, and volumes
docker-compose down --rmi all --remove-orphans

#make a backup of the map
cp -p "$VALHEIM_DATA"/worlds/"$ENVIRONMENT".db "$VALHEIM_DATA"/worlds/backups/"$ENVIRONMENT"_`date '+%Y_%m_%d__%H_%M_%S'`.bakdb
cp -p "$VALHEIM_DATA"/worlds/"$ENVIRONMENT".fwl "$VALHEIM_DATA"/worlds/backups/"$ENVIRONMENT"_`date '+%Y_%m_%d__%H_%M_%S'`.bakfwl

#restart and rebuild in case there are changes to the Dockerfile/docker-compose.yml
docker-compose up -d --build --force-recreate

#change to the directory holding the old saves so that the delete script doesn't have to look elsewhere
cd "$VALHEIM_DATA"/worlds/backups

#Remove all but 18 most recent files (last 7 days)
ls -tp | grep -v '/$' | tail -n +19 | xargs -I {} rm -- {}
