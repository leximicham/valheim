# Inspired by CanadaBry/ValheimDocker, nopor/docker-valheim, and lloesche/valheim-server-docker

FROM steamcmd/steamcmd:latest

MAINTAINER Lexi Micham <opensource@leximicham.com>

# download requirements
RUN apt-get update && \
	apt-get -y install --no-install-recommends libsdl2-2.0-0:i386

# start the server main script
ENTRYPOINT ["bash", "/valheim/scripts/start_server.sh"]
